import React, { Component, ChangeEvent, FormEvent, MouseEvent } from 'react';
import './../form.css';
import axios from 'axios';
import { string } from 'prop-types';

const API_PATH = 'http://localhost:3000/api/contact/contactform.php';

type Props = {id: string, requesttype: string, subject: string, children?: never}
interface State{
    name: string,
    email: string,
    subject: string,
    requesttype: string,
    message: string,
    mailsent: boolean,
    error: string,
}

class BaseForm extends Component<Props, State> {
    constructor(props: Props) {
        super(props);
        this.state = {
            name: '',
            email: '',
            subject: props.subject,
            requesttype: props.requesttype,
            message: 'Please write an messsage here.', 
            mailsent: false,
            error: '',               
        };
    }

    handleChange = (event: ChangeEvent<HTMLInputElement>):void  =>  {
        this.setState(
            {
                //value: event.target.value, 
                name: event.target.value,
                email: event.target.value,
                subject: event.target.value,
            });
    }    

    handleTextAreaChange = (event: ChangeEvent<HTMLTextAreaElement>):void => {
        this.setState({message: event.target.value})
    }

    handleSubmit = (event: MouseEvent<HTMLInputElement>):void => {
        event.preventDefault();           
        console.log(this.state);

        axios({
            method: 'post',
            url: /*`${process.env.REACT_APP_API}`*/ `${API_PATH}`,
            headers: {'content-type': 'application/json' },
            data: this.state
        })
        .then(result => {
            this.setState({
                mailsent: result.data.sent
            })
        })
        .catch(error => this.setState({error: error.message}));
    };
    
    render() {
        const { id, requesttype, subject } = this.props;

        return(
            <div>
                <p>Send request</p>
                <div>
                    <form action="#" >
                    <label>Name</label>
                    <input type="text" id="fname" name="firstname" placeholder="Your name.." 
                        onChange={this.handleChange}
                    />
                    {/* <label>Last Name</label>
                    <input type="text" id="lname" name="lastname" placeholder="Your last name.." /> */}
                
                    <label>Email</label>
                    <input type="email" id="email" name="email" placeholder="Your email" 
                        onChange={this.handleChange}
                    />                
                    <label>Subject</label>
                    <input type="text" id="subject" name="subject" value={subject} 
                        onChange={this.handleChange}
                    />                
                    <label>Request type</label>
                    <input type="text" id="requesttype" name="requesttype" value={requesttype} readOnly={true} 
                    />

                    <label>Message</label>
                    <textarea id="message" rows={15} name="message" value={this.state.message} onChange={this.handleTextAreaChange} />
                    <input type="submit" value="Submit" onClick={this.handleSubmit} />
                    <div>
                    {this.state.mailsent &&
                        <div>Thank you for contcting us.</div>
                    }
                    </div>                    
                    </form>
                </div>
            </div>
        );
    }
}

export default BaseForm;
