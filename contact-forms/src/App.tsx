import React from 'react';
import logo from './logo.svg';
import './form.css';
import BaseForm from './components/BaseForm';

const config = {
  api: `${process.env.REACT_APP_API}`
}

const App: React.FC = () => {
  return (
  <div className="App">
    <BaseForm /*config={config}*/ id="" requesttype="ttt" subject="sbj" />
  </div>
  );
}

export default App;
